<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserTypesController;
use App\Http\Controllers\RestikosController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('users', [UserController::class, 'getAll']);
Route::get('users/{id}', [UserController::class, 'getById']);
Route::post('users', [UserController::class, 'create']);
Route::post('users/{id}', [UserController::class, 'update']);
Route::delete('users/{id}', [UserController::class, 'delete']);

Route::get('userTypes', [UserTypesController::class, 'getAll']);
Route::get('userTypes/{id}', [UserTypesController::class, 'getById']);
Route::post('userTypes', [UserTypesController::class, 'create']);
Route::post('userTypes/{id}', [UserTypesController::class, 'update']);
Route::delete('userTypes/{id}', [UserTypesController::class, 'delete']);

Route::get('restikos', [RestikosController::class, 'getAll']);
Route::get('restikos/{id}', [RestikosController::class, 'getById']);
Route::post('restikos', [RestikosController::class, 'create']);
Route::post('restikos/{id}', [RestikosController::class, 'update']);
Route::delete('restikos/{id}', [RestikosController::class, 'delete']);