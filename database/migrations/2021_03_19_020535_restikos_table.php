<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RestikosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restikos', function (Blueprint $table){
            $table->id();
            $table->date("date");
            $table->text("do");
            $table->text("learned");
            $table->text("loved");
            $table->text("new");
            $table->text("problems");
            $table->text("objectives");
            $table->text("missed");
            $table->text("trainer");
            $table->text("success");
            $table->text("todo");
            $table->text("mood");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
