<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Restikos;

class RestikosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $restikos = 
        [
            [
"1",
"2020-11-03",
"1) Veille n°1: LG s’apprêterait à dévoiler un smartphone « enroulable »
2) Codage sur Bit bash + exercice
3) Conception physique de la machine à café",
"1) Faire une veille technologique
2) De nouvelles balises",
"L'ambiance de travail, apprendre de nouvelles balises, et le travail d'équipe.",
"Les balises:
- touch; créer mes fichiers
- cd; se déplacer
- mkdir créer des dossiers
- history; consulter l'historique des balises utilisées
@Brenda Kong ce ne sont pas des balises mais des commandes :)",
"Blocage pour renommer un fichier",
"1) Synthétiser la veille technologique.
2) Mener à bien un projet en équipe.",
"Rien en particulier",
"Rien",
"Oui",
"M'entraîner",
"4",
],
[
"2",
"2020-11-04",
"1) Veille n°2: Involontairement, une compagnie d’assurance a divulgué des millions de données personnelles aux GAFAM
2) Courte description de Simplon
3) Conception de l'arbre de compétences d'un développeur web sur papier et sur Canva",
"1) Prendre connaissance de Simplon
2) Apprendre à faire un arbre de compétences",
"Apprendre de nouvelle chose et les mettre en pratique",
"Mini entraînement sur bitbash avec certaines balises",
"Blocage avec certaines balises",
"Être assidue dans son travail",
"Rien du tout",
"Rien",
"Oui",
"Continuer à m'entrainer sur les bitbash",
"4",
],
[
"3",
"2020-11-05",
"1) Veille technologique n°3: Xiaomi a un plan pour devenir le nouveau roi de la photo (à la place de Huawei)
2) Initiation sur VS code et GitLab",
"1) Créer des branches et du contenu ",
"Réussir la création de contenu...",
"VS code et GitLab",
"Blocage sur VS code",
"Coder sur VS code",
"Rien",
"Rien",
"Oui",
"M'entraîner",
"4",
],
[
"4",
"2020-11-06",
"1) Veille technologique n°4: USA : submergés par la désinformation, les géants du web tentent de réagir
2) Présentation de groupe du projet machine à café",
"Quelques conseils pour faire une meilleure présentation en groupe",
"Travailler en équipe",
"Rien aujourd'hui",
"Aucune",
"S'améliorer sur la présentation",
"Rien ",
"Rien",
"Oui",
"M'entraîner",
"4",
],
[
"5",
"2020-11-09",
"1) Veille technologique n°5: La fintech attire les GAFA. L’Inde réagit par une politique antitrust
2) Jeu de groupe
3) Réalisation des wireframe du futur site de la formation DW",
"Ce qu'est un site catalogue (entre site vitrine et site e-commerce), un intranet (échange d'info en interne dans une organisation, entreprise…)",
"Jeu collaboratif",
"Moqups pour faire les wireframes",
"Moqups ne sauvegarde, n'y ne permet de partager la maquette",
"Faire collaborer l'ensemble des membres de l'équipe autour d'un même projet",
"De chance…? Perte de mon wireframe",
"Rien",
"A moitiée",
"Trouver une solution au problème de sauvegarde",
"4",
],
[
"6",
"2020-11-10",
"1) Veille technologique n°6: Amazon est dans le viseur de la Commission Européenne
2) Présentation des Framework du site de la formation
3) Initiation d'Adobe XD",
"Utiliser Adobe XD pour la création de la maquette d'un site web",
"Apprendre à utiliser Adobe XD",
"Adobe XD",
"Blocage sur Adobe XD",
"Apprendre à utiliser un nouvel outil de travail",
"Rien",
"Rien",
"Oui",
"M'entraîner sur l'outil",
"4",
],
[
"7",
"2020-11-12",
"1) Veille technologique n°7: L’interface d’Instagram change et ajoute deux nouveaux onglets
2) Avancement sur les moqups (soit DW + Log in)
3) Codage HTML",
"HTML",
"Bonne question !",
"HTML",
"Bien lire entre les lignes...!",
"Reproduire une page web HTML",
"De chance ? et d'attention",
"Rien",
"Oui",
"M'entraîner",
"4",
],
[
"8",
"2020-11-13",
"1) Veille technologique n°8: Voici ce qu’Apple pourrait faire pour éviter les accusations d’abus de position dominante
2) Terminer les Moqups Formation, Inscription et connexion + Espace perso.
3) Révélation des secrets",
"Les secrets de certains",
"L'ambiance de travail",
"Rien",
"Aucune",
"Terminer à temps",
"Rien",
"Rien",
"A moitiée",
"Continuer le site, le finir rapidement",
"4",
],
[
"9",
"2020-11-16",
"1) Veille technologique n°9: Le Bitcoin est arrivé sur PayPal, secousse à venir en 2021 ?
2) Sentence: Responsive design et Bootstraap
3) Codage HTML
3) ",
"Faire un carrousel avec HTML et utiliser Bootstrap",
"Réussir ",
"Bootstrap",
"Problème de codage...",
"Utiliser Bootstrap et coder",
"D'attention",
"Rien",
"Oui",
"Continuer d'en faire",
"4",
],
[
"10",
"2020-11-17",
"1) Veille technologique n°10: Google Maps : des données en temps réels sur la fréquentation des transports en commun
2) Sentence: Benchmark
3) Codage HTML sur le site de la formation
4) Révélation des secrets 2.0",
"Les secrets de certains et balises HTML",
"Réussir",
"Rien",
"Blocage sur le codage de la future page développeur web du site de la formation",
"Structurer et coder la page",
"Toujours d'attention",
"Rien",
"A moitiée",
"Continuer ",
"4",
],
[
"11",
"2020-11-18",
"1) Veille technologique n°11: Désinformation : les patrons de Facebook et Twitter sur le grill du Sénat
2) Sentence: Les CMS
3) Codage HTML sur le site de la formation
4) Clôture des secrets
4) Révélation des secrets 2.0",
"Créer un formulaire HTML",
"Faire quelque chose de propre",
"Rien",
"HTML",
"Avancer le code du site",
"Rien",
"Rien",
"Oui",
"Finaliser le tout avec les autres membres de l'équipe",
"4",
],
[
"12",
"2020-11-19",
"1) Veille technologique n°12: Facebook est accusé de forcer ses équipes à revenir au bureau malgré les risques sanitaires
2) Sentence: 
- La gamification
- Sujet mystère: Le leadership 
3) Codage HTML sur le site de la formation.",
"A rendre mon formulaire plus présentable",
"Montrer une interface plus esthétique",
"Rien aujourd'hui",
"Bouton s'inscrire pas assez lisible et peu esthétique",
"Finir le site dans les temps",
"De temps et de rapidité",
"Rien",
"Presque",
"Booster mon équipe ? Etre plus par derrière ?",
"4",
],
[
"13",
"2020-11-20",
"1) Veille technologique n°13: Vaccin contre la Covid-19 : Facebook, Google et Twitter allient leurs forces pour lutter contre la désinformation
2) Sentence: 
La suite Adobe
3) Présentation du site de la formation
4) Retours de la semaine + infos
5) Annonce des résultats du meilleur site de la formation",
"Gérer mon stresse lors de la présentation devant un jury",
"Etre surprise notamment lors de l'annonce des résultats... Surprenant !",
"Rien",
"Oublie par un membre de l'équipe le moqups de la page espace personnel",
"Présenter en groupe un projet et bien répartir le temps de parole",
"De rigueur",
"Rien",
"Oui",
"Faire le code de la page espace personnel",
"4",
],
[
"14",
"2020-11-23",
"1) Veille technologique n°14: Instagram : bientôt, un programme pour rémunérer les médias ?
2) Sentence: 
- LE SEO
- Discord
3) Week Starter
4) Création d'un CV sous forme de mini site en binôme",
"La fonction .row",
"Arriver à faire seule ce que j'ai envie",
"Rien de nouveau",
"Faire des blocs sur une même ligne",
"Commencer à créer un mini site web",
"Rien",
"Rien",
"Oui",
"Continuer sur cette lancée",
"4",
],
[
"15",
"2020-11-24",
"1) Veille technologique n 15: Facebook lance un outil de collecte pour les personnes dans le besoin.
2) Avancer dans SoloLearn.
3) Communiquer avec mes binômes et mon équipe. ",
"Pas grand chose aujourd’hui ",
"Le calme",
"Rien",
"Aucune",
"Maintenir un contact avec les camarades de la formation",
"De chance",
"Rien",
"Non",
"Bonne question",
"2",
],
[
"16",
"2020-11-25",
"Sentence n 16: Electric Days : l’énergie du futur se décline en version digitale le 1er décembre",
"De plus en plus de marques, de groupes ou d’enseignes mettent en place des événements en ligne pour éviter des regroupements d’individus sur un même lieu.",
"Me reposer ",
"Rien",
"Aucune",
"Pas d’objectif aujourd’hui ",
"Rien",
"Rien",
"Non",
"...",
"0",
],
[
"17",
"2020-11-26",
"1) Veille technologique n°17: Apple rémunère des influenceurs TikTok pour promouvoir son iPhone 12 Mini
2) Sentence: Les URL
3) Codage du site CV
4) Clôture des secrets
4) Révélation des secrets 2.0",
"Les URL",
"Rien",
"Rien",
"Aucune",
"Finir le site CV",
"Rien",
"Rien",
"Oui",
"...",
"0",
],
[
"18",
"2020-11-27",
"1) Veille technologique n°18: PlayStation Plus Collection : Des joueurs bannis pour avoir débloqué les jeux sans PS5
2) Sentence: FTP et clients FTP
3) Présentation en binôme des site CV
4) Speed coding 
4) Clôture des secrets",
"Bootstrap",
"Rien",
"Rien",
"J'ai pas réussi à aligner mes cards",
"Coder en 1h ",
"Je sais pas",
"Rien",
"A moitiée",
"M'entraîner",
"4",
],
[
"19",
"2020-11-30",
"1) Veille technologique n°19: Samsung Galaxy Note : finalement, un dernier modèle en 2021 ?
2) Week starter
3) Sentence: Les préprocesseurs
4) Projet noël",
"Mieux coder",
"Finir le projet noel",
"Rien",
"Ne pas réussir a tout faire en 1 journée",
"Finir une projet en binôme",
"Rien",
"Rien",
"Oui",
"M'améliorer",
"4",
],
[
"20",
"2020-12-01",
"1) Veille technologique n°20: Facebook aurait déboursé un milliard de dollars pour racheter Kustomer, spécialiste en gestion de la relation client
2) RDV pour le site Code In Mind et le projet noël
3) Sentence: Minification et CDN
4) Training - JS #1 
5) Codage projet noël",
"Mieux coder avec javscript",
"Réussir mon code",
"Javascript",
"Problème de balise fermante div",
"Réussir l'exercice code avec javascript",
"D'attention ?",
"Rien",
"Oui",
"M'entrainer avec javascript et avancer au plus vite les projets",
"4",
],
[
"21",
"2020-12-02",
"1) Veille technologique n°21: Avec son nouveau fil d’actualité, Google Maps va faire de l’ombre à Facebook
2) Sentence: CamelCase et autres méthodes de codage
3) Training - JS #2
4) Codage projet noël",
"A générer des div avec javascript",
"Réussir l'exo",
"Font-awesome",
"Générer des div",
"Finir l'exo sur javascript et avancer le projet de noël",
"Suivre le rythme du cours de codage",
"Aller moins vite",
"A moitié",
"Finir le projet noël et le site de la formation",
"4",
],
[
"22",
"2020-12-03",
"1) Veille technologique n°22: Et si Xiaomi devançait tous ses concurrents en présentant le Mi 11 ce mois de décembre ?
2) Sentence: jQuery
3) Training - JS #3 
4) Avancement du projet Noël",
"Relier un bouton a une fonction JavaScript pour l'animé",
"Réussir l'exo",
"Rien il me semble",
"Table ",
"Finir le projet Noël dans les temps",
"Rien",
"Rien, parfait ",
"Oui",
"Finir le projet Noël au plus tôt et finaliser l'événement Noël pour le 18/12",
"4",
],
[
"23",
"2020-12-04",
"1) Veille technologique n°23: Google Play Music vient de disparaître pour de bon, adieu.
2) Sentence: UX design et UI design
3) Avancement du projet Noël
4) Week Review",
"Javascript",
"L'aide des autres",
"start",
"Les icons ne sont pas de la même taille que les cases du tableau",
"Avancer au maximum le projet noël",
"Je sais pas",
"Rien",
"Oui",
"Finir le projet Noël au plus tôt et finaliser l'événement Noël pour le 18/12",
"4",
],
[
"24",
"2020-12-07",
"1) Veille technologique n°23: Google demande des règles claires, sans pour autant freiner l’innovation
2) Week Starter
3) Sentence: Qu'est ce qu'un hébergement ? Ou comment héberger son site ? Webhosting
4) Finalisation de projet noël",
"Ajouter de l'audio sur mon code HTML",
"Le rendu du projet",
"<audio>",
"Finir à temps les projets en cours dans les temps",
"Finir à temps les projets en cours dans les temps",
"Rien",
"Rien",
"Oui",
"Travail sur le site de la fomation",
"4",
],
[
"25",
"2020-12-08",
"1) Veille technologique n°23: Google Messages, bientôt aussi sécurisé que WhatsApp et Telegram ?
2) Sentence: Les API
3) Finalisation de projet noël
4) Finalisation des pages statique du site Code In Mind",
"APIs",
"La sentence captivante de Erai",
"Rien je crois",
"Finir ma page développeur web",
"Finir les projets en cours ",
"Rien",
"Rien",
"Oui",
"Améliorer ma page si j'ai le temps",
"4",
],
[
"26",
"2020-12-09",
"1) Veille technologique n°24: Apple menace de bannir les apps qui traquent les utilisateurs sans obtenir un consentement
2) Sentence: Le fil d'Ariane
3) Cours sur les BDD",
"Comment fonctionne une BDD",
"Le cours de BDD",
"Bibliothèque de Airtable, Mon API sur Airtable",
"Réussir à suivre le cours et avoir le même résultat",
"Suivre le Cours de BDD",
"Rien",
"Rien",
"Oui",
"Finir l'évènement du 18/12",
"4",
],
[
"27",
"2020-12-10",
"1) Veille technologique n°25: La CNIL inflige des amendes de 100 millions € à Google et de 35 millions € à Amazon
2) Sentence: 
- Méthode Agile VS traditionnelle
- Les environnements de codage
3) Cours sur les BDD #2",
"Remplacer pour relier à la base basse",
"Bien voir le tableau",
"filterByFormula",
"Terminer le Trello et faire fédérer tout le monde autour du même projet",
"Faire fédérer les membres de l'équipe sur un même projet",
"Rien",
"Rien",
"Oui",
"Embellir notre projet noël ",
"4",
],
[
"28",
"2020-12-11",
"1) Veille technologique n°26: Google prend des mesures contre les fausses informations sur les vaccins
2) Sentence: 
- Les environnements de codage
- La matrice d'Eisenhower
3) Week Review
4) Finalisation du projet noël",
"A faire tomber de la neige",
"Faire tomber la neige",
"inline-block pour le bouton de la neige",
"Relier le bouton à la neige",
"Finaliser le morpion",
"De la pratique en JS",
"RIen",
"Oui",
"Améliorer le morpion",
"4",
],
[
"29",
"2020-12-14",
"1) Veille technologique n°27: Un problème technique chez Google a perturbé ses principaux services (Gmail, YouTube…) dans le monde entier
2) Sentence: Pomodoro
3) Week Starter
4) Préparation de ma sentence",
"A quoi servait un pomodoro et quelques règles de codage",
"L'ambiance générale",
"Rien",
"Perte de motivation dans mes tâches",
"Finir dans les temps ce que j'ai  faire",
"Rien",
"Rien",
"Oui",
"Finaliser ma sentence pour demain",
"3",
],
[
"30",
"2020-12-15",
"1) Veille technologique n°28: L’application TikTok aussi sur certaines Smart TV Samsung
2) Sentence: Mind Mapping
3) Wamp Server
4) Préparation de ma sentence",
"Redirection de google vers monpremiersite.com",
"Mind mappiing",
"Wamp Server et fichier hosts",
"Remettre sur google.com",
"Finir ma sentence",
"De temps et de motivation",
"Rien",
"Non",
"M'entrainer pour ma sentence",
"3",
],
[
"31",
"2020-12-16",
"1) Veille technologique n°29: Netflix débarque sur un nouveau support dès aujourd’hui
2) Sentence: Les règles de codage
3) Site CodeInMind
4) Projet Noël",
"Les règles de codage",
"Finaliser les projets",
"mt-4...",
"Mettre en quinconce",
"Finir a temps les projets en cours",
"Rien je crois",
"Rien",
"Oui",
"M'entraîner sur JS",
"4",
],
[
"32",
"2020-12-17",
"1) Veille technologique n°30: Zoom : pas de limite de 40 minutes durant les fêtes de fin d’année
2) Sentence: Les versions logiciels
3) Finalisation et présentation du site CideInMind",
"Conseils pour améliorer le site de la formation",
"La sentence sur les versions logiciels",
"Slide sur la présentation plus pro",
"Se coordonner pour la présentation en groupe",
"Présentation du site CodeInMind",
"D'entraînement pour une présentation au top",
"Rien",
"Oui",
"Améliorer mes lacunes",
"4",
],
[
"33",
"2021-01-11",
"1) Veille technologique n°31: Apple et Hyundai pourraient rapidement signer un partenariat et présenter un véhicule d’ici 2022
2) Sentence: Scheme color
3) Avancement de l'application veille/restiko
4) Réunion de pilotage CodeInMind",
"La théorie des couleurs et ajax",
"La reprise",
"API airtable",
"L'affichage de ma base dans mon tableau",
"Utiliser JS ",
"D'attention ",
"Rien",
"Oui",
"Finir mon application",
"4",
],
[
"34",
"2021-01-12",
"1) Veille technologique n°32: Twitter annonce la suppression de 70 000 comptes liés à QAnon
2) Sentence: scheme color
3) Speed Coding 
4) Finalisation du site codeinmind",
".container
.row
colonne",
"A rendre un travail plus ou moins (visuellement) correct",
"Simplon",
"Push on the master",
"Speed coding",
"Rien je crois",
"Huuuuum je sais pas",
"Oui",
"Finir en 2h le speed coding d'aujourd'hui",
"4",
],
[
"35",
"2021-01-13",
"1) Veille technologique n°33: Razer dévoile un masque futuriste avec ventilateur, lumières et amplificateur de voix
2) Sentence: PHP
3) Speed Coding ",
"Maitrise des colonnes",
"L'activité du midi",
"gitlab page",
"backgound color width",
"Ne pas tricher et trouver une solution valide",
"Sais pas",
"Rien",
"Oui",
"Finir un speed coding",
"3",
],
[
"36",
"2021-01-14",
"1) Veille technologique n°34: La CNIL sanctionne le ministère de l’Intérieur pour avoir utilisé des drones lors du premier confinement
2) Sentence: MVC
3) Finalisation de l'application veille/restiko 
4) Site codeinmind: bloc img + button link...",
"Utiliser api airtable pour 
lier et ajouter sur ma BDD",
"Rien",
"API airtable",
"Le contenu de ma page ne s'affiche pas",
"Réussir à afficher et rendre mon bouton fonctionnel pour qu'il ajoute à ma base",
"De savoir",
"...",
"Bof Bof",
"Finir mon app",
"3",
],
[
"37",
"2021-01-15",
"1) Veille technologique n°35: En 2020, la pandémie permet au secteur des applications mobiles une expansion impressionnante
2) Finalisation de l'application veille/restiko  ",
"L'utilité de php pour éviter les répétitions html (exemple: navbar identique sur toutes les pages du site)",
"La démo de Tinihau",
"kraken.io vue mais pas utilisé",
"function ",
"Fluidité avec JS",
"De pratique",
"Rien",
"A moitiée",
"Pratiquer plus JS",
"3",
],
[
"38",
"2021-01-18",
"1) Veille technologique n°36: Apple dispose désormais d’un prototype de smartphone pliable
2) Sentence: Cahier des charges / Les cardinalités
3) Avancement du site codeinmind",
"Les cardinalités en BDD",
"L'ambiance",
"Rien",
"J'arrive pas à mettre la fonction pour modifier",
"Réussir à supprimer et modifier ma veille et mon restiko",
"De connaissance",
"Rien",
"Pas trop",
"Rendre dans les temps les projets",
"2",
],
[
"39",
"2021-01-19",
"1) Veille technologique n°37:  Jedi Blue » : au-delà du contrat publicitaire, un symptôme du problème des GAFA
2) Sentence: Les spécificités fonctionnelles et techniques / BREAD/CRUD
3) Avancement de l'application veille/restiko",
"Les spécificités fonctionnelles et techniques / BREAD/CRUD",
"Les sentences qui étaient très clair",
"Rien ",
"Lenteur dans l'avancement des projets",
"Être plus fluide et rigoureux dans ses projets",
"De rigueur",
"Sais pas",
"Pas trop",
"Me motiver",
"2",
],
[
"40",
"2021-01-20",
"1) Veille technologique n°38: Zoom sur MeWe, le réseau social « anti-Facebook » en plein boom
2) Réunion CodeInMind
3) Avancement de l'application veille/restiko",
"Rien",
"Rien",
"Rien",
"Tout",
"Tout finir les délais ",
"Tout",
"Je sais pas",
"Non",
"Tout",
"2",
],
[
"41",
"2021-01-21",
"1) Veille technologique n°39: Taxe GAFA : l’administration Biden semble prête à trouver un accord global
2) Sentence: FineTech...
3) Avancement de l'application veille/restiko",
"Qu'on pouvait mettre du html dans du script (et ajuster). J'ai compris les functions, et j'ai appris les qu'on pouvait ne pas utiliser les ID mais les class, les class parents...",
"Le cours particulier ^^",
".show 
.hide...",
"Tout, trop, pleins...",
"Reproduire la même chose pour le restiko",
"Aucune idée",
"Faire plus souvent des séances individuels",
"Je pense que oui",
"Reproduire la même chose pour le restiko",
"4",
],
[
"42",
"2021-01-22",
"Avancement dans sur mon application restiko",
"Bien concaténer pour la lisibilité",
"Les cookies au pépites de chocolat ",
"Rien de nouveau je crois",
"Concaténation",
"Finir la partie restiko",
"De temps peut être ",
"Pas grand chose à dire a son sujet",
"A moitié ",
"Finir mon app",
"3",
],
[
"43",
"2021-01-25",
"1) Veille technologique n°41: Pourquoi la France ne peut pas “rater le virage” des technologies quantiques
2) Avancement de l'application st valentin",
"Revoir comment rédiger un cahier des charges",
"Mes co-équipiers",
"Rien je crois",
"Gérer plusieurs projets",
"Gérer plusieurs projets",
"Gestion du temps",
"Pas comme d'habitude",
"Oui",
"Mieux gérer mon temps",
"3",
],
[
"44",
"2021-01-26",
"1) Veille technologique n°42: Test Galaxy S21 Ultra : Samsung a trouvé sa formule magique
2) Cours mySQL
3) Texte index codeinmind
4) Sentence: Code d'états HTTP",
"Revoir comment concevoir un persona",
"L'ambiance",
"Rien",
"Gestion du temps",
"Avancer dans les projets",
"Gestion des tâches, du temps",
"Rien",
"Oui",
"Mieux gérer mon temps",
"3",
],
[
"45",
"2021-01-27",
"1) Veille technologique n°43: Shorts : la réponse de YouTube à TikTok cartonne en bêta
2) Cours mySQL + PHP
3) Sentence: B2B, B2C, C2C... la pagination et les jointures 
4) Moqup page accueil de l'app st valentin ",
"Jointure php",
"Le cours ",
"php my admin",
"my sql txt error",
"Equilibrer le temps sur les différents projet",
"De temps et de motivation",
"Etre moins rapide",
"Non",
"gestion du temps et des tâches",
"2",
],
[
"46",
"2021-01-28",
"1) Veille technologique n°44:  L’administration Biden souhaite reconsidérer les sanctions contre Huawei
2) Cours SSH
3) Sentence: SSH
4) Page sentence du site codeinmind (no good, no push)",
"clé public et privé SSH",
"la sentence SSH",
"créer localhost de mon app veille/restiko",
"Reproduire le même schema de ma veille sur la page sentence JS de codeinmind",
"Réussir ma problématique",
"De brain mahana",
"Rien",
"A moitié",
"Je envie de créer une BDD sur mySQL...",
"2",
],
[
"47",
"2021-01-29",
"1) Veille technologique n°45: Mark Zuckerberg ne veut pas que la politique gâche votre expérience sur Facebook
2) Cours SSH
3) Sentence: SSH
4) Page sentence du site codeinmind (no good, no push)",
"merge on the master to push...",
"La suite",
"Rien",
"php",
"BDD",
"Manque de visuel pour le projet st vanlentin",
"Sais pas",
"Oui",
"Avancer l'app st valentin ",
"3",
],
[
"48",
"2021-02-01",
"1) Veille technologique n°46: Sur liste noire du département de la Défense des États-Unis, Xiaomi riposte avec une action devant les tribunaux américains.
2) Réunion site codeinmind
3) index de l'app animara matcher
4) Modif de la table username + code",
"Peut pas mettre un bouton sur une image",
"Mon index",
"uplabs",
"Aucune moqup pour l'app st valentin",
"Finir les autres page de l'app st valentin",
"De visuel",
"Rien",
"Oui",
"Faire les pages restantes de l'app st valentin",
"3",
],
[
"49",
"2021-02-02",
"1) Veille technologique n°47: Google doit verser 3,8 millions de dollars après des accusations de discriminations raciales et sexistes
2) Instruction à appliquer pour la suite de index2
3) Avancement dans le projet st valentin: index et co",
"Que la co internet pouvait perturber code to git",
"La sentence qui nous serra pratique ",
"Rien ",
"Remémorer les col pour la page fil d'actualité",
"Finir le front End des projets en cours serait bien",
"Rien en particulier si ce n'est de l'assiduité",
"Rien",
"Presque ",
"La problématique",
"2",
],
[
"50",
"2021-02-03",
"1) Veille technologique n°48: Apple vise 100 000 Apple Car par an et investirait 3 milliards d'euros dans Kia pour y arriver
2) Cours php + jointure
3) Avancement de la page index 2 du site codeinmind",
"Les jointures SQL",
"Réussir à comprendre une partie",
"Les jointures",
"Les jointures",
"Les jointures",
"De bons yeux ou de mire regarder ?",
"...",
"Oui",
"Avancer au mieux le code du site (au moins ma partie) pour aider mes coéquipiers.",
"3",
],
[
"51",
"2021-02-04",
"1) Veille technologique n°49: 
2) Cours rsync
3) Intervention extérieur: Ali
4) Création table articles blog public et privé",
"BDD avec php",
"QLes explications de Tehau pour la BDD",
"include php",
"Retenir ce qu'il a dit et le reproduire",
"Reproduire ph sur les autres pages",
"Rien",
"Rien",
"Je pense que oui",
"Lier la BDD sur les autres page",
"4",
],
[
"52",
"2021-02-05",
"1) Veille technologique n°50: YouTube ajoute une nouvelle section dédiée au sport
2) App St valentin
3) Week review
",
"Rien",
"Rien",
"Rien",
"Trop",
"Finir à temps...",
"Sais pas",
"Rien",
"Oui",
"Etre présente",
"2",
],
[
"53",
"2021-02-08",
"1) Veille technologique n°51: Concurrence : voici comment la Chine veut dompter ses géants de la Tech
2) Tuto #1 app meilleur codeur (doc drive + trello + XD)
3) Page fil d'actualité php de codeinmind
",
"include php",
"Le tuto",
"Rien",
"N'arrive pas à commit",
"Maitriser git",
"...",
"Rien à signaler",
"A moitié",
"Lier la base sur la page fil d'actualité",
"2",
],
[
"54",
"2021-02-09",
"1) Veille technologique: Facebook renforce sa lutte contre la désinformation sur la Covid-19 et les vaccins
2) Tuto #2 app meilleur codeur (doc drive + trello + XD)
3) BDD veille_restiko
",
"include php",
"Le tuto",
"Création d'un BBD, tables...",
"importer ma table airtable en php",
"importer ma table airtable en php",
"Rien",
"Rien",
"A moitié",
"Passer d'une base airtable à php",
"2",
],
[
"55",
"2021-02-10",
"1) Veille technologique: Dopé par la soif d’actualité des internautes, Twitter continue de grandir
2) Tuto #3
3) Connexion à ma BDD",
"Exportation CVS airtable + importation SQL + connexion à ma BDD ",
"Explications + démo",
"Connexion à la BDD",
"Fonctionne à moitiée",
"Connexion à la BDD",
"...",
"Beaucoup d'infos à assimiler en très peu de temps",
"Non",
"Lier à la BDD",
"2",
],
[
"56",
"2021-02-11",
"1) Veille technologique: Donald Trump banni à vie de Twitter
2) Aller voir Teremu pour demander de l'aide
3) Connexion à la BDD du projet animara matcher",
"Insert",
"Les explications du formateur ",
"PHP",
"Reproduire",
"Reproduire sur une autre application",
"De compréhension ",
"Rien a signaler",
"A moitié",
"Lier mon app a la BDD veille_restiko",
"3",
],
[
"57",
"2021-02-12",
"Présentation de notre projet animara matcher devant un jury de développeurs ",
"Axe d'amélioration pour le projet e",
"Les conseils pour améliorer notre app",
"Rien",
"Rajouter les fonctions dites par les développeurs ?",
"Finir l'app ",
"De chance ?",
"Rien",
"A moitié",
"Mon app veille",
"2",
],
[
"58",
"2021-02-15",
"1) Veille technologique: Pourquoi Mark Zuckerberg veut « faire du mal » à Apple
2) Réunion codeinmind
3) Sentence: WYSIWYG
4) Cours PHP",
"La méthodo quand on fait face à soucis",
"Le cours",
"Rien",
"Reproduire ce qui à été dit en cours",
"Reproduire ce qui à été dit en cours",
"D'attention je dirai",
"Rien",
"Oui",
"Lier mon app a la BDD veille",
"3",
],
[
"59",
"2021-02-16",
"1) Veille technologique: Perseverance sur Mars : Avant de toucher Mars, le rover devra en passer par sept minutes de terreur
2) Cours app meilleur codeur
3) Commencer le code de ma veille",
"Mise en place du vote",
"Le cours",
"Rien",
"Rendre l'application my_veille vendredi ainsi que la vidéo de présentation du contenu de l'espace privé de codeinmind",
"Rendre l'application my_veille vendredi ainsi que la vidéo de présentation du contenu de l'espace privé de codeinmind",
"De motivation",
"Rien",
"Oui",
"Avancer l'application my_veille ainsi que la vidéo de présentation",
"2",
],
[
"60",
"2021-02-17",
"1) Veille technologique: 
2) Meeting Evans
3) App Veille",
"Rien",
"Le meeting",
"Je sais que j'ai appris de nouvelles fonctionnalités mais je ne n'en rappelle pas",
"Oulala, pleins, pleins...",
"Faire la vidéo de présentation du site codeinmind",
"De motivation",
"Rien",
"Oui",
"Finaliser l'app my_veille",
"3",
],
[
"61",
"2021-02-18",
"1) Veille technologique: 
2) Réunion codeinmind
3) Faire l'email pour la co de l'espace privée
4) App Veille",
"Mettre une card en overfloat lors du passage de la souris",
"Ma veille qui s'affiche ",
"Mettre une card en overfloat lors du passage de la souris et faire afficher la veille correctement avec id",
"Rendre le tout joli",
"Rendre le tout joli",
"De volonté pour faire la vidéo",
"...",
"Oui",
"Rendre le tout joli",
"4",
],
[
"62",
"2021-02-22",
"1) Veille technologique: LinkedIn pourrait créer une plateforme pour connecter les indépendants aux entreprises
2) Week starter
3) Sentence: Les options Rsync
4) Réunion codeinmind",
"...",
"Réunion avec des instructions clairs",
"Rien",
"Aucun",
"App veille à finir et objectifs codeinmind à atteindre",
"Rien",
"Explications clairs",
"Oui",
"App veille à finir et objectifs codeinmind à atteindre",
"3",
],
[
"65",
"2021-02-23",
"1) Veille technologique: Les pirates piratés: la Chine aurait espionné les États-Unis grâce à un outil de la NSA
2) Sentence: Le phishing, le recettage et le no SQL
3) Avancement dans le rangement de fichier...",
"Le phishing, le recettage et le no SQL",
"Les sentences: Le phishing et le recettage facile à comprendre",
"Rien",
"Aucun",
"Objectifs codeinmind",
"Rien",
"Rien",
"Oui",
"Finir le rangement du reste des pages: css, js, img et css",
"3",
],
[
"63",
"2021-02-24",
"1) Veille technologique: Les informations personnelles de 500 000 patients français ont fuité sur le web
2) Sentence: Les flux RSS et Les modèles économiques
3) Training - PHP: L'upload des fichiers
4) App Veille: Page index adaptable au format mobile+ Bug de ma navbar mobil + Bug du boutton consulter (renvoie sur la veille suitante)",
"Uploader une image même si pas j'ai pas le code en tête",
"Le training pour upload les images ",
"Régler le bug de mon boutton consulter",
"Avancer sur codeinmind",
"Codeinmind + faire la partie privé de mon app veille",
"Un peu de tout je dirai",
"Training top",
"A moitié",
"Objectifs codeinmind de la semaine",
"3",
],
[
"72",
"2021-02-25",
"1) Veille technologique: 
2) Sentence: Les notifications push
3) Training - API: Création d'une API Pokemon",
"Créer une API",
"Le training creation d'une API",
"...",
"Adapter l'API pour codeinmind",
"Adapter l'API pour codeinmind",
"...",
"Super training",
"A moitié",
"Atteindre les objectifs et préparer ma sentence NPM",
"3",
],
[
"73",
"2021-02-26",
"1) Veille technologique: Que retenir du Pokémon Presents de cet après-midi ? (Legendes Pokemon Arceus, Pokemon Diamant et Perle…)
2) Week review
3) Ajax des pages privé codeinmind",
"Ajax",
"La patience et le côté pédagogue de Erai",
"Ajax - templates et models codeinmind privé",
"Le faire pour l'ensemble des pages",
"Finir pour l'ensemble des pages ce weekend",
"De volonté",
"Rien",
"Oui",
"Finir pour l'ensemble des pages ce weekend",
"3",
],
[
"74",
"2021-03-01",
"1) Veille technologique: Google avertit des millions d’utilisateurs que Google Photos peut endommager vos photos
2) Week starter
3) Duel meilleur codeur de la semaine: Tehau VS Erai
4) Création d'un compte sur infinity pour héberger ma BDD + liaison + mise en ligne (échec)
5) Vérification des models et templates pour CodeInMind private ",
"J'ai bien capté grâce a Erai le comment du pourquoi (Ajax)",
"Tjr sa patience et les explications claires, et le duel de codeur ",
"Infinity",
"Creds pas bon ",
"Faire afficher mes veilles ",
"Je sais pas",
"Rien a signaler",
"Oui",
"- Régler certains détails pour CodeInMind private et passer a la suite
- Faire afficher mes veilles ",
"3",
],
[
"75",
"2021-03-02",
"1) Veille technologique: IA : les Etats-Unis veulent maintenir leur domination technologique face à la Chine
2) Rendre propre mes models et templates
3) Réussir a afficher mes veilles (SOS Teremu)
4) Commencer mon flux RSS",
"Attention aux guillemets pour les images",
"Huuum bonne question, je ne saurais quoi dire ",
"Flux RSS",
"Flux RSS",
"Flux RSS",
"Aucune idée",
"Il m'a aidé a faire afficher mes veilles ",
"Oui",
"Flux RSS",
"4",
],
[
"76",
"2021-03-03",
"1) Veille technologique: IA : TikTok crée une nouvelle instance pour améliorer sa modération en Europe
2) avancer mon flux RSS pour faire afficher une seule veille
3) ré créer un côté sur freemyhosting pour ma BDD",
"Afficher la veille d'un jour ou pas ordre du plus récent ",
"Rien",
"Rien",
"Ça n'affiche pas comme ça devrait le faire",
"Y arrive seule",
"Sais pas",
"Merci d'être patient ",
"Non",
"Finir",
"2",
],
[
"77",
"2021-03-04",
"1) Veille technologique : Phishing : Des pirates créent des mails alarmistes signés Microsoft pour dérober vos données
2) Réunion CodeInMind
3) Modification du code
4) Week review",
"###NAME### Pour afficher l'élément",
"Rien",
"###NAME### Pour afficher l'élément",
"Le faire pour tte les pages",
"Le finir pour lundi et rendre visible mes images my_veille",
"Vérifier plus tôt my_veille",
"Rien ",
"A moitié",
"Réaliser les objectifs",
"2",
],
[
"79",
"2021-03-08",
"1) Veille technologique: Joe Biden recrute un conseiller partisan du démantèlement des GAFA
2) Week Starter
3) Préparation Sentence: NPM
4) Speed coding 4: upload de photos",
"App Upload de fichier dans mon ordi",
"Rien",
"Upload d'images sur mon ordi",
"Upload sur une BDD",
"Être au point pour la démo NPM",
"Mauvaise gestions de mes projets",
"Aller le voir pour ma sentence pas du tt au point ",
"Pas trop je dirai",
"Faire une démo NPM plus ou moins potable et compréhensible ",
"2",
],
[
"81",
"2021-03-09",
"1) Veille technologique: Des start-up françaises attaquent Apple pour non-respect de la législation sur la publicité ciblée
2) Préparation Sentence: NPM
3) MCD sur la BDD",
"Relation MCD okay",
"Rien du tout",
"gitignore",
"gitignore",
"Sentence au top",
"J'en sais trop rien",
"Il m'a aidé pour régler un bug sur mon ordi et à installer les paquets ",
"Oui",
"Préparer le discours",
"3",
],
[
"82",
"2021-03-10",
"1) Veille technologique: Censurées sur Instagram, des féministes poursuivent Facebook en justice et dénoncent un deux poids, deux mesures",
"2) Sentence: NPM, Tokens, node
3) avancement sur le front end du re design de feedly + Back pas au point du tout",
"L'utilité d'utiliser un token, et a quoi servait index.js dans la sentence node",
"La sentence Token",
"API pour notre projet feedly",
"Le Back End, la BDD, mon flux RSS et le front pas du tt au point... J'ai mal au crâne ",
"Finir ça aujourd'hui ",
"De m'y prendre plus tôt ?",
"J'aurai peut être du aller le voir... ",
"Non ",
"Régler mon flux et régler les autres problèmes ",
"1",
],
[
"83",
"2021-03-11",
"1) Veille technologique: L’incendie d’un data center OVH fait état d’un bilan catastrophique
2) Challenge: rôle d'arbitre
3) Avancement projet feedly -> API",
"afficher le flux (dbh au lieu de pdo) ",
"Pas grand chose",
"login à revoir",
"login et API",
"Finir à 2 le projet",
"De temps, de communication, de cohésion...",
"Rien",
"Non",
"Assurer la présentation du projet",
"2",
],
[
"84",
"2021-03-12",
"1) Veille réalisé sur App feedly
2) présentation du projet feedly 
3) week review",
"L'importance de bien structurer une présentation pour éviter le cafouillage ",
"Rien",
"Rien",
"Présenté un truc 2h avant la présentation a partir de presque rien, et comment se faire entendre par quelqu'un qui ne t'entends et t'écoute pas ",
"Zen",
"Tout",
"Rien a signaler ",
"Non",
"Mettre en ligne et envoyer les liens sur Simplon ",
"1",
],
[
"86",
"2021-03-15",
"1) Veille: Google risque 5 milliards de dollars pour avoir pisté les utilisateurs de Chrome en mode privé
2) Week starter
3) Apprendre à utiliser Laravel en créant des API",
"Installation de laravel",
"Facilité d'utilisation avec laravel",
"Laravel installateur ",
"La version de composer doit être la même que wamp sinon impossibilité d'installer laravel",
"Utiliser laravel sur les projets",
"De tout",
"Il m'a aidé a résoudre mon problème ",
"A moitié",
"Suivre la suite du cours #laravel",
"3",
],
[
"88",
"2021-03-16",
"1) Veille: Pourquoi Facebook entraîne ses intelligences artificielles avec vos vidéos
2) Bandanaz
3) Training laravel #2",
"Comment créer une table, la compléter et envoyer sur la BDD avec Laravel",
"Le training",
"Fichier migration et autres + de nouvelles commandes laravel",
"Mon inattention ? Facilement déconcentré",
"Reproduire la même chose a la maison",
"D'attention ",
"Il nous a aidé",
"Oui",
"Poursuivre le training et reproduire",
"3",
],
[
"90",
"2021-03-17",
"1) Veille: Piratage de Twitter : un ado de 17 ans condamné à trois ans de prison
2) Bandanaz
3) Training laravel #3
4) Rapel JS avec Tehau teacher",
"Uppdate, create and delete with laravel and see with postman",
"Le training laravel de Teremu et les rappels JS de Tehau",
"Update, create and delete with laravel and see with postman",
"Attention au petites erreurs",
"Reproduire sur les autres projets",
"D'attention ",
"Super traianing",
"Oui",
"M'entrainer en reproduisant la même choses sur les autres projets",
"3",
],
[
"91",
"2021-03-18",
"1) Veille: TikTok a trouvé un moyen de retenir toujours plus ses utilisateurs
2) Cours CSS 
3) Bandanaz 
4) Training laravel #5",
"Mettre une fonction dans model pour l'utiliser plusieurs fois dans le controller",
"Le trainong CSS et le cours laravel #5",
"- Mettre une fonction dans model pour l'utiliser plusieurs fois dans le controller
- Hasher le MDP",
"migration",
"Reproduire pour la table UserType et Restikos",
"Beaucoup de choses",
"Super cours",
"A moitiée",
"Finir l'API restikos",
"3",
],
[
"92",
"2021-03-19",
"1) Veille: Facebook dévoile un prototype d'interface neuronale au poignet pour (presque) contrôler la machine par la pensée
2) Training laravel #6 API myveille
3) Week Review",
"Le cheminement de la création d'une API ",
"Le cours",
"Update, create and delete with laravel and see with postman",
"Bug",
"Finir API Restikos",
"Trop ou pas assez de s",
"Rien à signaler",
"Oui",
"Finir API Restikos",
"3",
],
[
"93",
"2021-03-22",
"1) Veille: Trump paré à faire son retour sur les réseaux sociaux, mais avec sa propre plateforme
2) Week Starter 
3) Bandanaz vs meilleur codeur
4) Training laravel #7 API myveille",
"- Table pivot -> Relation NM
- Boucle for dans une autre boucle for",
"Le cours et l'entraide",
"BelongToMany pour les relations",
"Ne voit pas super bien le tableau",
"Se recentré sur codeinmind et faire les requête avec L'API",
"De bon yeux peut-être ?",
"Rien",
"Je pense que oui",
"Codeinmind: Faire les requête avec L'API",
"3",
],
[
"94",
"2021-03-23",
"1) Veille: Microsoft semble s'intéresser à Discord et jusqu'à envisager un rachat à hauteur de 10 milliards de dollars.
2) Cours PHP + Laravel: App et API todolist
3) Bandanaz 
4) Rappel HTML",
"Utiliser with pour faire des relations croisées ",
"Rappel HTML",
"Rappel des scripts",
"Je me suis perdu vers la fin avec App et l'API ",
"Finir l'App et l'API todolist",
"De concentration ",
"Rien a signaler",
"A moitié",
"Finir l'App et l'API todolist",
"2",
],
[
"95",
"2021-03-24",
"1) Veille: Comment acheter des Bitcoins ? Le guide du débutant.
2) Cours Laravel: App et API todolist
3) Bandanaz 
4) Correction du HTML avec Tehau et Marie",
"J'ai compris l'idée de l'App todolist mais pas le code en tête ",
"L'ambiance et le cours",
"- Insérer un wrapper
- importance des tâches 
- ajouter, une date et une heure dans un modal
- 1 user avec catégories et taches ",
"Le finir",
"Le finir",
"De le finir chez moi",
"C'était chaud a suivre vers la fin",
"A moitié",
"Le finir a la maison par moi même ",
"2",
],
[
"96",
"2021-03-25",
"1) Veille: FACEBOOK, INSTAGRAM, WHATSAPP ET MESSENGER TOUCHÉS PAR UNE PANNE
2) Bandanaz
3)  Avancement project my_veille App-API",
"Concaténer mes veilles dans le seeder",
"La vidéo training laravel #6 by Klein",
"Concaténer mes veilles dans le seeder",
"Motivation en baisse",
"Relier l'app à l'API ",
"Motivation",
"Rien a signaler",
"A moitiée",
"Finir ce que j'ai commencé avec l'app-API my_veille",
"2",
],
[
"97",
"2021-03-26",
"1) Veille: Facebook met fin à une opération de cyberespionnage visant les ouïghours hors de Chine
2) Cours App-API restikos
3) Week Review",
"Pagination du restikos",
"Le cours sur comment lier l'App a l'API (Ajax)",
"Ajax -> restiko",
"Refaire sur mon app",
"Refaire sur mon app toute seule",
"Motivation",
"Super cours",
"A moitiée",
"Refaire sur mon app restiko toute seule",
"2",
],
        ];

        foreach($restikos AS $tmp):
            $restiko = [
                'date' => $tmp[1],
                'do' => $tmp[2],
                'learned' => $tmp[3],
                'loved' => $tmp[4],
                'new' => $tmp[5],
                'problems' => $tmp[6],
                'objectives' => $tmp[7],
                'missed' => $tmp[8],
                'trainer' => $tmp[9],
                'success' => $tmp[10],
                'todo' => $tmp[11],
                'mood' => $tmp[12],
            ];
            Restikos::create($restiko);
        endforeach;
    }
}
