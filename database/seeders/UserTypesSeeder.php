<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserTypes;


class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersTypes = [
            [
                "name" => "administrateur",   
            ],
            [
                "name" => "utilisateur",   
            ],
            [
                "name" => "visiteur",   
            ]
        ];

        foreach($usersTypes AS $userType):
            UserTypes::create($userType);
        endforeach;
    }
}
