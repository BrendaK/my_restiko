<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                "lastname" => "Liqueur",
                "firstname" => "Lili",
                "email" => "lililiqueur@gmail.com",
                "user_type_id" => 1,
            ],
            [
                "lastname" => "lala",
                "firstname" => "lola",
                "email" => "lolalala@gmail.com",
                "user_type_id" => 2,

            ]
        ];

        foreach($users AS $user):
        $user["password"] = Hash::make("secret");
            User::create($user);
        endforeach;
    }
}
