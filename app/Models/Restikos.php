<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restikos extends Model
{
    use HasFactory;

    public static function new($restiko, $request){
        $restiko->date = $request->date;
        $restiko->do = $request->do;
        $restiko->learned = $request->learned;
        $restiko->loved = $request->loved;
        $restiko->new_use = $request->new_use;
        $restiko->problems = $request->problems;
        $restiko->goal = $request->goal;
        $restiko->missed = $request->missed;
        $restiko->former = $request->former;
        $restiko->objectives_achieved = $request->objectives_achieved;
        $restiko->will_do = $request->will_do;
        $restiko->note_on_5 = $request->note_on_5;
        $restiko->user_type = $request->user_type;

        $restiko->save();

        return $restiko;
    }
}
