<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use\App\Models\Restikos;

class RestikosController extends Controller
{
    public function getAll(){
        $restikos =Restikos::paginate(12);
        return Response::json($restikos, 200);
    }

    public function getById($id){
        $restiko =Restikos::find($id);
        return Response::json($restiko, 200);
    }

    public function create(Request $request){
        // if(!$request->confirm_password) return Response::json("confirm_password missing", 500);

        $restiko =new Restikos;

        $restiko = Restikos::new($restiko, $request);

        return Response::json($restiko, 200);
  
    }

    public function update($id, Request $request){
        $restiko = Restikos::find($id);
        
        $restiko = Restikos::new($restiko, $request);

        return Response::json($restiko, 200);

    }

    public function delete($id){
        Restikos::destroy($id);
        return Response::json("record deleted", 200);
    }
}
